
#!/bin/bash

# Function to check if a command exists
command_exists() {
    command -v "$1" >/dev/null 2>&1
}

# Function to install a package
install_package() {
    PACKAGE_NAME=$1
    echo "$PACKAGE_NAME not found. Do you want to install it? (y/n)"
    read -r response
    if [[ "$response" == "y" ]]; then
        echo "Installing $PACKAGE_NAME..."
        apt-get update && apt-get install -y "$PACKAGE_NAME"
    else
        echo "$PACKAGE_NAME is required to continue. Exiting..."
        exit 1
    fi
}

# Install prerequisites if not installed
if ! command_exists git; then
    install_package git
fi

if ! command_exists node; then
    curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
    install_package nodejs
fi

# Prompt user for additional dependencies
echo "The script checks and installs git, node, and npm. Is there any specific other dependency you would like to install? (Type 'n' to skip or the name of the dependency)"
read -r additional_dependency

if [[ "$additional_dependency" != "n" ]]; then
    install_package "$additional_dependency"
fi

# Clone the GitHub repository
REPO_URL="https://github.com/rwieruch/node-express-server-rest-api"
if [ ! -d "node-express-server-rest-api" ]; then
    echo "Cloning repository..."
    git clone $REPO_URL
fi

# Change to the repository directory
cd node-express-server-rest-api

# Copy the setup script into the repository directory
cp ../setup.sh .

# Install required JS dependencies
echo "Installing dependencies..."
npm install

# Load .env file to system environment variables
if [ -f .env ]; then
    export $(grep -v '^#' .env | xargs)
    echo ".env file loaded."
else
    echo ".env file not found. Exiting..."
    exit 1
fi
